# importe required libraries
import pandas as pd
import os
import json
import shutil

file_path = os.path.realpath(__file__)
file_path=file_path.split('\\')[:-1]
file_path='\\'.join(file_path)
config_file_path=file_path+"\\config_file.json"


#read config file
try:
    f = open(config_file_path)
    config_data = json.load(f)
    outupt_path = config_data['outupt_path']
    bulk_files_flg = config_data['bulk_files_flg']
    input_file_path = config_data['input_file_path']
    input_file_name = config_data['input_file_name']
    archive_path = config_data['archive_path']
    f.close()   
except Exception as read_config_error:
    print("Error in reading config file" + str(read_config_error))
    

'''
#config inputs
outupt_path = "Desktop/Pilot Projects/xlxs to csv converter/csv_files/"
bulk_files_flg = "Y"
input_file_path = "Desktop/Pilot Projects/xlxs to csv converter/xlsx_files/"
input_file_name = "Desktop/Pilot Projects/xlxs to csv converter/xlsx_files/test.xlsx"
'''


def xlsx_to_csv_converter(input_file_nm: str,outupt_path: str):
    try:
        xls = pd.ExcelFile(input_file_nm)
        for i in range(len(xls.sheet_names)):
            df = pd.read_excel(xls, xls.sheet_names[i])
            df.to_csv (outupt_path+xls.sheet_names[i]+ ".csv", index = None, header=True)
    except Exception as xlsx_to_csv_converter_error:
        print("Error in converting xlsx to csv" + str(xlsx_to_csv_converter_error))
    return True
   

if __name__ == "__main__":
    try:    
        if bulk_files_flg=="Y":
            print("Processing Multiple Files")
            dir_list = os.listdir(input_file_path)  
            if (len(dir_list)):
                for file_nm in dir_list:
                    if file_nm.endswith(".xlsx"): #filter only xlsx files
                        file_name=input_file_path+"/"+file_nm
                        res = xlsx_to_csv_converter(file_name,outupt_path)
                        if(res):
                            shutil.move(file_name, archive_path+"/"+file_nm)
            else:
                raise Exception("Input Path is empty") 
        elif bulk_files_flg=="N":
            print("Processing Specific File")
            res = xlsx_to_csv_converter(input_file_name,outupt_path)
            if(res):
                file_nm=input_file_name.split('/')[-1]
                shutil.move(input_file_name, archive_path+"/"+file_nm)
    except Exception as main_func_error:
        print("Error in main function" + str(main_func_error))