## Project's Title : 
#### XLSX TO CSV CONVERTER

## Project Description : 
#### This application will converter all the sheets inside given XLSX file into multiple CSV files. 
#### It can process a single xlsx file or bulk xlsx files. 
#### After processing it will archive the xlsx files. 
#### Filter out only xlsx files in case of bulk processing is required

## How to Install and Run the Project :
#### Download the zip in your local
#### Vanish the mentioned folders - xlsx_files, csv_files and archive_files
#### Put your xlsx file(s) in "xlsx_files" directory
#### Update the config file with reqired details (Details description given below) - 
##### - outupt_path - Path of "csv_files" directory
##### - bulk_files_flg - "Y" if processing of multiple files required, "N" if processing of specific file required
##### - input_file_path - Provide the directory path if multiple files processing is required
##### - input_file_name - Provide the xlsx file path if single file processing is required
##### - archive_path - Provide the path of archive directory
#### run xlsx_to_csv_converter.py in python 3.7 or above version
#### CSV files can be found under "csv_files" directory